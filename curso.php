<?php
require "db_config.php";
require "config/helper.php";
require "config/url.class.php";
require "config/functions.php";
$URI = new URI();

$url = explode("/", $_SERVER['REQUEST_URI']);
$get_url = $url[2];
$get_url_2 = "";

$stmt = $DB_con->prepare("SELECT * FROM products");
$stmt->execute();
if ($stmt->rowCount() > 0) {
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		extract($row);
		$string1 = remove_symbols_accents(utf8_decode($get_url));
		$string2 = remove_symbols_accents(utf8_decode($id));
		if ($string1 == $string2) {
			$get_url_2 = $id;
		}
	}
}

$stmt = $DB_con->prepare("SELECT * FROM products where id='$get_url_2'");
$stmt->execute();
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	extract($row);
}
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
	<?php include "components/heads.php"; ?>
	<script src="https://cdn.tailwindcss.com"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
  <link rel="stylesheet" href="./assets/css/swiper.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
  <script>
    tailwind.config = {
      theme: {
        extend: {
          colors: {
            color1: '#006CAC',
            color2: '#FFFFFF',
            color3: '#0070AC',
          }
        }
      }
    }
  </script>
</head>

<body>
	<?php include "components/navbar.php"; ?>
	<div style="border-radius: 25px;" class="mx-auto max-w-6xl pt-2 bg-white px-6 py-8 mt-4">
		  <div class="rounded-2xl">
            <h3 class="mb-4 text-xl text-center font-black py-2"><?php echo $name; ?></h3>
          </div>
		<div class="flex justify-center">
		<img class="rounded-md" src="<?php echo $URI->base("/admin/uploads/products/$img"); ?>">
		</div>
		<div class="pt-2">
			<?php echo $info ?>
		</div>
		<div>
		<div class="flex justify-center mt-4">
            <a href="<?php echo $link ?>" target="blank" class="text-<?php echo $btn_color ?> bg-<?php echo $btn_bg ?> focus:ring-4 rounded-md font-bold text-xl px-5 py-2 text-center">Inscreva-se</a>
          </div>
		</div>
	</div>
	<?php include "components/footer.php"; ?>

</body>

</html>