<?php
require "db_config.php";
require "config/helper.php";
require "config/url.class.php";
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php include "components/heads.php"; ?>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css" />
  <link rel="stylesheet" href="./assets/css/swiper.css">
</head>


<body>
  <?php include "components/navbar.php"; ?>
  <div class="mx-auto max-w-6xl pt-2">
    <div class="lg:pt-4 lg:p-0 lg:p-10 p-5">
      <h2 class="mb-4 lg:text-xl text-justify">
        O INSTITUTO AKDEMUS surgiu com a missão de fomentar o desenvolvimento através do <span class="font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color3">conhecimento</span>, capacitando profissionais e levando às organizações <span class="font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color3">soluções tecnológicas</span> e boas práticas na administração pública e privada.
      </h2>
      <h2 class="mb-4 lg:text-xl text-justify">
        Ofertamos serviços de treinamentos, congressos, capacitação profissional, pós-graduações nas áreas jurídicas e das ciências sociais, com o intuito de elevar a qualificação profissional dos participantes.
        Atuamos com ética, zelo e contamos com a colaboração dos mais renomados especialistas para ofertar a máxima qualidade dos eventos desenvolvidos.
        Contem conosco para agregar ainda mais conhecimento para um desenvolvimento contínuo e sustentável.
      </h2>
      <h2 class="mb-4 lg:text-xl text-justify">
      <span class="font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color3">MISSÃO</span><br>
        Fomentar o desenvolvimento através do conhecimento, capacitando profissionais e levando às organizações soluções tecnológicas e boas práticas na administração pública e privada.
      </h2>
      <h2 class="mb-4 lg:text-xl text-justify">
      <span class="font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color3">VISÃO</span><br>
        Ser reconhecido regional e nacionalmente pelo empenho e qualidade na oferta de serviços de treinamentos, congressos, capacitação profissional, pós-graduações em diversas áreas e eventos que levem à sociedade o conhecimento essencial para um desenvolvimento contínuo e sustentável.
      </h2>
      <h2 class="mb-4 lg:text-xl text-justify">
      <span class="font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color3">VALORES</span><br>
        O INSTITUTO AKDEMUS atua com ética e zelo pelas informações adquiridas e/ou transmitidas a seus clientes, no intuito de atingir a máxima qualidade dos eventos que se propõe a desenvolver.
      </h2>
    </div>
    <iframe width="100%" height="400" src="https://www.youtube.com/embed/8YzInrtTWmU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
  <?php include "components/footer.php"; ?>
</body>

</html>