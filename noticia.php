<?php
require "db_config.php";
require "config/helper.php";
require "config/url.class.php";
require "config/functions.php";
$URI = new URI();

$url = explode("/", $_SERVER['REQUEST_URI']);
$get_url = $url[2];
$get_url_2 = "";

$stmt = $DB_con->prepare("SELECT * FROM posts");
$stmt->execute();
if ($stmt->rowCount() > 0) {
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		extract($row);
		$string1 = remove_symbols_accents(utf8_decode($get_url));
		$string2 = remove_symbols_accents(utf8_decode($id));
		if ($string1 == $string2) {
			$get_url_2 = $id;
		}
	}
}

$stmt = $DB_con->prepare("SELECT * FROM posts where id='$get_url_2'");
$stmt->execute();
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	extract($row);
}
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
	<?php include "components/heads.php"; ?>
</head>

<body>
	<?php include "components/navbar.php"; ?>
	<div style="border-radius: 25px;" class="mx-auto max-w-6xl pt-2 bg-white px-6 py-8 mt-6">
    <div class="rounded-2xl">
            <h3 class="mb-4 text-xl text-center font-black py-2"><?php echo $title; ?></h3>
          </div>
		<div class="flex justify-center">
		<img class="rounded-md" src="<?php echo $URI->base("/admin/uploads/posts/$img"); ?>">
		</div>
		<div class="pt-2">
			<?php echo $info ?>
		</div>
		<div>
		<div class="flex justify-center mt-4">
            <a href="<?php echo $URI->base(""); ?>" class="bg-color1 text-white focus:ring-4 rounded-md font-bold text-xl px-5 py-2 text-center">Voltar para o inicio</a>
          </div>
		</div>
	</div>
	<?php include "components/footer.php"; ?>

</body>

</html>