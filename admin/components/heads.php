<title>Dashboard Akdemus</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="" name="keywords">
<meta content="" name="description">
<meta name="author" content="Cairo Felipe Developer">

<meta property="og:title" content="" />
<meta property="og:url" content="" />
<meta property="og:image" content="" />
<meta property="og:description" content="" />

<link rel="stylesheet" href="./assets/css/style.css">
<link href="../assets/img/logo.png" rel="icon">
<link href="../assets/img/logo.png" rel="apple-touch-icon">
<script src="https://cdn.tailwindcss.com"></script>