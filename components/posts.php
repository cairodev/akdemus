<section class="swiper swiper_products products max-w-screen-xl mx-auto pt-10 mt-2" id="noticias">
  <h1 class="lg:text-5xl text-2xl text-center mb-4"><span class="font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color3">NOTÍCIAS</span></h1>
  <div class="swiper-wrapper">
    <?php $stmt = $DB_con->prepare("SELECT * FROM posts order by id desc");
    $stmt->execute();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      extract($row);
    ?>
      <div class="swiper-slide">
        <div class="<?php echo $bg; ?> max-w-lg p-6 mx-auto rounded-3xl shadow_csc">
          <div class="rounded-2xl">
            <h3 class="mb-4 text-xl font-black py-2"><?php echo $title; ?></h3>
          </div>
          <div>
            <img class="rounded-md" src="./admin/uploads/posts/<?php echo $img; ?>">
          </div>
          <div class="product_info">
            <?php echo $info ?>
          </div>
          <div class="flex justify-center">
            <a href="<?php echo $URI->base('noticia/' . slugify($id)); ?>" class="text-<?php echo $btn_color ?> bg-<?php echo $btn_bg ?> focus:ring-4 rounded-md font-bold text-xl px-5 py-2 text-center">Saiba mais</a>
          </div>
        </div>
      </div>
    <?php
    }
    ?>
  </div>
</section>