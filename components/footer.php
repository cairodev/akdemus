<footer class="bg-color1 mt-10" id="footer">
  <div class="max-w-6xl p-4 py-6 mx-auto lg:py-16 md:p-8 lg:p-10">
    <div class="grid grid-cols-1 gap-8 md:grid-cols-3 lg:grid-cols-4">
      <div>
        <div class="justify-center flex">
          <img src="<?php echo $URI->base('/assets/img/logo_branca.png') ?>">
        </div>
      </div>
      <div>
        <h3 class="mb-6 text-sm font-semibold text-white uppercase">
          WWW.AKDEMUS.COM.BR
        </h3>
        <ul class="text-white">
          <li class="mb-4">
            <a href="#" class="hover:underline">Cursos</a>
          </li>
          <li class="mb-4">
            <a href="#" class="hover:underline">Quem Somos</a>
          </li>
          <li class="mb-4">
            <a href="#" class="hover:underline">Contato</a>
          </li>
        </ul>
      </div>
      <div>
        <h3 class="mb-6 text-sm font-semibold text-white uppercase">Contato</h3>
        <p class="text-white">
          Telefone: 86 99401-2491
          Endereço: R. Áurea Freire, 1443 - Jóquei, Teresina - PI, 64049-160, Brasil
          E-mail: institutoakdemus@gmail.com
        </p>
      </div>
      <div>
        <a href="https://api.whatsapp.com/send?phone=5586994012491" target="blank"><i class="bi bi-whatsapp"></i></a>
        <a href="https://www.instagram.com/institutoakdemus/" target="blank"><i class="bi bi-instagram"></i></a>
        <i class="bi bi-facebook"></i>
      </div>
    </div>
    <div class="text-center pt-10">
      <span class="block text-base text-center text-white">© 2023 - INSTITUTO AKDEMUS
      </span>
      <span class="block text-xs text-center text-white">DE ESTUDOS AVANÇADOS EM CIÊNCIAS SOCIAIS E JURÍDICAS
      </span>
      <div class="pt-10">
        <span class="block text-xs text-center text-white">Site criado por
        </span>
        <span class="block text-xs text-center text-white">Web Developer Full Stack: @cairofelipedev
        </span>
      </div>
    </div>
  </div>
</footer>

<script src="<?php echo $URI->base('/assets/js/dark_mode.js') ?>"></script>
<script src="https://unpkg.com/flowbite@1.4.1/dist/flowbite.js"></script>