<div class="mx-auto max-w-6xl pt-2 about">
  <div class="items-center lg:grid lg:grid-cols-2">
    <div class="lg:p-10 p-2">
      <img class="w-full" src="<?php echo $URI->base("/assets/img/logo.png"); ?>" alt="Akdemus" />
    </div>
    <div class="lg:pt-4 lg:p-0 lg:p-10 p-5">
      <h2 class="mb-4 lg:text-xl text-justify">
        O INSTITUTO AKDEMUS surgiu com a missão de fomentar o desenvolvimento através do <span class="font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color3">conhecimento</span>, capacitando profissionais e levando às organizações <span class="font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-color1 to-color3">soluções tecnológicas</span> e boas práticas na administração pública e privada.
      </h2>
      <div class="flex justify-center">
        <a href="#" class="border-2 border-color1 bg-<?php echo $btn_color ?> text-<?php echo $btn_bg ?> focus:ring-4 rounded-md font-bold text-xl px-5 py-2 text-center">Saiba mais</a>
      </div>
    </div>
  </div>
  <iframe width="100%" height="400" src="https://www.youtube.com/embed/8YzInrtTWmU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>